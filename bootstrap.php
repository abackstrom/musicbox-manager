<?php

require 'vendor/autoload.php';
require 'src/Musicbox/Loader.php';

$loader = new \Musicbox\Loader();
$loader->register();

$loader->addNamespace('Musicbox', __DIR__ . '/src/Musicbox');

$di = new Pimple\Container();
$di['router'] = function($di) {
    return new Klein\Klein();
};

$di['db'] = function($di) {
    return new PDO(sprintf('sqlite:%s/musicbox.sqlite3', __DIR__));
};

$di['songs'] = function($di) {
    return new Musicbox\Db\Songs($di);
};

$di['twig'] = function($di) {
    $loader = new Twig_Loader_Filesystem(__DIR__ . '/templates');
    return new Twig_Environment($loader, [
        'cache' => '/tmp/musicbox/templates',
        'debug' => true,
        'strict_variables' => true,
    ]);
};

$app = new Musicbox\App($di);
$app->configureRoutes();
$app->run();
