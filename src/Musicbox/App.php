<?php

namespace Musicbox;

use Klein\Request, \Klein\Response;
use PDO;

class App
{
    private $di;

    public function __construct(\Pimple\Container $di)
    {
        $this->di = $di;
    }

    public function configureRoutes()
    {
        $this->di['router']->respond('GET', '/', [$this, 'index']);
    }

    public function run()
    {
        $this->di['router']->dispatch();
    }

    public function index(Request $request, Response $response)
    {
        $songs = $this->di['songs']->getSongs();

        $tpl = $this->di['twig']->loadTemplate('index.twig');
        return $tpl->render([]);
    }
}
