<?php

namespace Musicbox\Db;

use Musicbox\Song;

class Songs
{
    private $di;

    public function __construct($di)
    {
        $this->di = $di;
    }

    public function getSongs()
    {
        $result = $this->di['db']->prepare('SELECT rowid, path, track, album, artist FROM songs');
        $result->execute();

        while ($row = $result->fetch()) {
            $song = new Song();

            $song->setPath($row['path']);
            $song->setAlbum($row['album']);
            $song->setTrack($row['track']);
            $song->setArtist($row['artist']);

            yield $song;
        }
    }
}
