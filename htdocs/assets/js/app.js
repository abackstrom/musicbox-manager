window.Musicbox = Ember.Application.create();
Musicbox.ApplicationAdapter = DS.FixtureAdapter.extend();

Musicbox.Router.map(function() {
    this.resource('songs', { path: '/' });
});

Musicbox.SongsRoute = Ember.Route.extend({
    model: function() {
        return this.store.find('song');
    }
});

Musicbox.Song = DS.Model.extend({
    title: DS.attr('string')
});

Musicbox.Song.FIXTURES = [
    { id: 1, title: 'Paranoid Android' },
    { id: 2, title: 'Mushaboom' },
    { id: 3, title: 'Baby Beluga' }
];
